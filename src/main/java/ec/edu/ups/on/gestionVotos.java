package ec.edu.ups.on;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.dao.TituloDao;
import ec.edu.ups.dao.VotosDAO;
import ec.edu.ups.modelos.CandidatoVotos;
import ec.edu.ups.modelos.Titulo;
import ec.edu.ups.modelos.Voto;

@Stateless
public class gestionVotos  implements gestionVotosLocal{
	//Aquí se inyectan los DAOS
	//Se hace un Inject por cada DAO
	@Inject
	private TituloDao tituloDao;
	
	private CandidatoVotos candidatoVotos;
	private List<CandidatoVotos> listaVotos = new ArrayList<CandidatoVotos>();
	
	@Inject VotosDAO vDao;
	
	@Override
	public List<CandidatoVotos> listarVotos(String titulo) {
		for (Object candidato : tituloDao.listarCandidatos(titulo)) {
			candidatoVotos = new CandidatoVotos();
			candidatoVotos.setCandidato(tituloDao.getNombreCandidato(Integer.valueOf(String.valueOf(candidato))));
			candidatoVotos.setVotos(tituloDao.contarVotos(String.valueOf(candidato)));
			listaVotos.add(candidatoVotos);
		}
		return listaVotos;
	}

	@Override
	public String vota(String cedula, String nombre, int candidato) {
		return tituloDao.votar(cedula, nombre, candidato);
	}

	@Override
	public List<Voto> listarVotosGenerales() {
		return vDao.listarVotos();
	}

	@Override
	public void registrarEleccion(Titulo titulo) {
		tituloDao.registrarEleccion(titulo);
	}

	@Override
	public Titulo getTitulo(int id) {
		return tituloDao.getTitulo(id);
	}

}
