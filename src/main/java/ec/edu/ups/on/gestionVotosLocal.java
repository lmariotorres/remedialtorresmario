package ec.edu.ups.on;

import java.util.List;

import javax.ejb.Local;

import ec.edu.ups.modelos.CandidatoVotos;
import ec.edu.ups.modelos.Voto;
import ec.edu.ups.modelos.Titulo;

@Local
public interface gestionVotosLocal {
	public List<CandidatoVotos> listarVotos(String titulo);
	public String vota(String cedula, String nombre, int candidato) throws Exception;
	public List<Voto> listarVotosGenerales();
        public void registrarEleccion(Titulo titulo);
        public Titulo getTitulo(int id);
}
