package ec.edu.ups.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.edu.ups.modelos.Voto;

@Stateless
public class VotosDAO {
	@PersistenceContext
	private EntityManager em;
	
	public List<Voto> listarVotos() {
		String consulta = "select v from Voto v";
		Query q = em.createQuery(consulta, Voto.class);
		return q.getResultList();
	}
}
