package ec.edu.ups.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sound.midi.Soundbank;

import ec.edu.ups.modelos.Candidato;
import ec.edu.ups.modelos.CandidatoVotos;
import ec.edu.ups.modelos.Titulo;
import ec.edu.ups.modelos.Voto;

@Stateless
public class TituloDao {
	@PersistenceContext
	private EntityManager em;
	
	private Voto voto = new Voto();
	
	public List<Integer> listarCandidatos(String titulo){
		String jpql = "select c.can_id from Candidato c WHERE c.can_titulo="+titulo;
		Query q = em.createQuery(jpql, Integer.class);
		return q.getResultList();
	}
	
	public String getNombreCandidato(int candidato) {
		String nomQuery = "select c.can_nombre from Candidato c WHERE c.can_id="+String.valueOf(candidato);
		return em.createQuery(nomQuery, String.class).getSingleResult();
	}
	
	public Integer contarVotos(String candidato) {
		try {
			String votosQuery = "select count(v) from Voto v WHERE v.vot_candidato="+String.valueOf(candidato);
			return Integer.valueOf(String.valueOf(em.createQuery(votosQuery, Long.class).getSingleResult()));
		} catch (Exception e) {
			System.out.println("Se ah producido la siguiente excepción: "+e.toString());
			return null;
		}
	}
	
	public String votar(String cedula, String nombre, int candidato) {		
		try {
			String jpql = "select v from Voto v WHERE v.vot_cedula="+cedula+" and v.vot_candidato = "+candidato;			
			if(em.createQuery(jpql, Voto.class).getResultList().isEmpty()) {
				voto.setCedula(cedula);
				voto.setNombre(nombre);
				voto.setVot_candidato(em.find(Candidato.class, candidato));
				em.persist(voto);
				return "Éxito al registrar el voto!!";
			}else {
				return "Ud ya ah votado por este candidato en esta lista";
			}
		} catch (Exception e) {
			return "Se ah producido el siguiente error: "+e.toString();
		}
	}
	
	public Titulo getTitulo(int id) {
		return em.find(Titulo.class, id);
	}
	
	public void registrarEleccion(Titulo titulo) {
		em.persist(titulo);
	}
}
