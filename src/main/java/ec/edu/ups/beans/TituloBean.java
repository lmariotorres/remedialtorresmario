package ec.edu.ups.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

import ec.edu.ups.modelos.Candidato;
import ec.edu.ups.modelos.CandidatoVotos;
import ec.edu.ups.modelos.Titulo;
import ec.edu.ups.on.gestionVotosLocal;

@ManagedBean
@SessionScoped
public class TituloBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4520483820494978127L;

	@Inject
	private gestionVotosLocal gestion;
	
	private String titulo;
	
	private String vigencia;
	
	private List<Candidato> candidatos;
	
	private String nombre;
	
	private String cedula;
	
	private String lista;
	
	private String telefono;
	
	private int idTitulo;
	
	public void RegistrarEleccion() {
		if(!candidatos.isEmpty()) {
			Titulo t = new Titulo();
			t.setTit_nombre(this.titulo);
			t.setTit_fec_vigencia(vigencia);
			t.setTit_candidatos(candidatos);
			gestion.registrarEleccion(t);	
			candidatos.removeAll(candidatos);
		}
	}
	
	@PostConstruct
	public void init() {
		candidatos = new ArrayList<Candidato>();
	}
	
	public void agregarCandidato() {
            
            System.out.println(this.nombre);
		Candidato c = new Candidato();
		c.setCan_nombre(this.nombre);
		c.setCan_cedula(this.cedula);
		c.setCan_lista(this.lista);
		c.setCan_telefono(this.telefono);
		candidatos.add(c);
		
	}
	
	private List<CandidatoVotos> listaVotos = new ArrayList<CandidatoVotos>();
	
	public List<CandidatoVotos> getListaVotos() {
		return listaVotos;
	}


	public void setListaVotos(List<CandidatoVotos> listaVotos) {
		this.listaVotos = listaVotos;
	}


	public gestionVotosLocal getGl() {
		return gestion;
	}


	public void setGl(gestionVotosLocal gl) {
		this.gestion = gl;
	}


	public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public List<CandidatoVotos> listarVotos() {
		listaVotos.removeAll(listaVotos);
		//listaVotos = gestion.listarVotos(titulo);
		return listaVotos;
	}

	public gestionVotosLocal getGestion() {
		return gestion;
	}


	public void setGestion(gestionVotosLocal gestion) {
		this.gestion = gestion;
	}


	public String getVigencia() {
		return vigencia;
	}


	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public List<Candidato> getCandidatos() {
		return candidatos;
	}

	public void setCandidatos(List<Candidato> candidatos) {
		this.candidatos = candidatos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getLista() {
		return lista;
	}

	public void setLista(String lista) {
		this.lista = lista;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public int getIdTitulo() {
		return idTitulo;
	}

	public void setIdTitulo(int idTitulo) {
		this.idTitulo = idTitulo;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "TituloBean [gestion=" + gestion + ", titulo=" + titulo + ", vigencia=" + vigencia + ", listaVotos="
				+ listaVotos + "]";
	}
}
