package ec.edu.ups.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import ec.edu.ups.modelos.Voto;
import ec.edu.ups.on.gestionVotosLocal;

@ManagedBean
@ViewScoped
public class VotosBean {
	@Inject private gestionVotosLocal gestionVotosLocal;
	
	private List<Voto> listaVotos;
	private int contador = 0;
	
	public gestionVotosLocal getGestionVotosLocal() {
		return gestionVotosLocal;
	}

	public void setGestionVotosLocal(gestionVotosLocal gestionVotosLocal) {
		this.gestionVotosLocal = gestionVotosLocal;
	}

	public List<Voto> getListaVotos() {
		return listaVotos;
	}

	public void setListaVotos(List<Voto> listaVotos) {
		this.listaVotos = listaVotos;
	}

	public int getContador() {
		return contador;
	}

	public void setContador(int contador) {
		this.contador = contador;
	}

	public List<Voto> listarVotos(){
		return gestionVotosLocal.listarVotosGenerales();
	}
	
	public String contar() {
		contador++;
		return null;
	}
}
