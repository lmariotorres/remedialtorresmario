package ec.edu.ups.modelos;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Votos")
public class Voto {
	@Id
	@Column(name="vot_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int vot_id;
	
	@Column(name="vot_nombre")
	private String nombre;
	
	@Column(name="vot_cedula")
	private String vot_cedula;
	
	@ManyToOne
	@JoinColumn(name="vot_candidato")
	private Candidato vot_candidato;

	public int getVot_id() {
		return vot_id;
	}

	public void setVot_id(int vot_id) {
		this.vot_id = vot_id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCedula() {
		return vot_cedula;
	}
	
	public void setCedula(String cedula) {
		this.vot_cedula = cedula;
	}

	public Candidato getVot_candidato() {
		return vot_candidato;
	}

	public void setVot_candidato(Candidato vot_candidato) {
		this.vot_candidato = vot_candidato;
	}

	@Override
	public String toString() {
		return "Voto [vot_id=" + vot_id + ", nombre=" + nombre + ", cedula=" + vot_cedula + ", vot_candidato="
				+ vot_candidato + "]";
	}
}
