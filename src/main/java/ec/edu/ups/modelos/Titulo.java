package ec.edu.ups.modelos;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Titulos")
public class Titulo {
	@Id
	@Column(name="tit_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int tit_id;

	@Column(name="tit_nombre")
	private String tit_nombre;
	
	@Column(name="tit_fec_vigencia")
	private String tit_fec_vigencia;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "can_titulo")
	private List<Candidato> can_titulo;

	public int getTit_id() {
		return tit_id;
	}

	public void setTit_id(int tit_id) {
		this.tit_id = tit_id;
	}

	public String getTit_nombre() {
		return tit_nombre;
	}

	public void setTit_nombre(String tit_nombre) {
		this.tit_nombre = tit_nombre;
	}

	public String getTit_fec_vigencia() {
		return tit_fec_vigencia;
	}

	public void setTit_fec_vigencia(String tit_fec_vigencia) {
		this.tit_fec_vigencia = tit_fec_vigencia;
	}
	
	public List<Candidato> getTit_candidatos() {
		return can_titulo;
	}

	public void setTit_candidatos(List<Candidato> tit_candidatos) {
		this.can_titulo = tit_candidatos;
	}

	@Override
	public String toString() {
		return "Titulo [tit_id=" + tit_id + ", tit_nombre=" + tit_nombre + ", tit_fec_vigencia=" + tit_fec_vigencia
				+ "]";
	}
}
