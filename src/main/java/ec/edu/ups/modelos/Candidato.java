package ec.edu.ups.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Candidatos")
public class Candidato {
	@Id
	@Column(name="can_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int can_id;

	@Column(name="can_cedula")
	private String can_cedula;
	
	@Column(name="can_nombre")
	private String can_nombre;
	
	@Column(name="can_telefono")
	private String can_telefono;
	
	@Column(name="can_lista")
	private String can_lista;

	public int getCan_id() {
		return can_id;
	}

	public void setCan_id(int can_id) {
		this.can_id = can_id;
	}

	public String getCan_cedula() {
		return can_cedula;
	}

	public void setCan_cedula(String can_cedula) {
		this.can_cedula = can_cedula;
	}

	public String getCan_nombre() {
		return can_nombre;
	}

	public void setCan_nombre(String can_nombre) {
		this.can_nombre = can_nombre;
	}

	public String getCan_telefono() {
		return can_telefono;
	}

	public void setCan_telefono(String can_telefono) {
		this.can_telefono = can_telefono;
	}

	public String getCan_lista() {
		return can_lista;
	}

	public void setCan_lista(String can_lista) {
		this.can_lista = can_lista;
	}
}
