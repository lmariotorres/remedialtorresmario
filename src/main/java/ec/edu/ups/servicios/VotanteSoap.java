package ec.edu.ups.servicios;

import java.util.List;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import ec.edu.ups.modelos.Voto;
import ec.edu.ups.on.gestionVotosLocal;

@WebService
public class VotanteSoap {
	@Inject
	private gestionVotosLocal gestionVotosLocal;
	private Voto voto = new Voto();
	
	@WebMethod
	public String votar(String cedula, String nombre, int candidato){
		try {
			return(gestionVotosLocal.vota(cedula, nombre, candidato));
		} catch (Exception e) {
			System.out.println("Se ah producido el siguiente error: "+e.toString());
			return "Ah ocurrido un error al ingresar su voto";
		}
	}
}
